package ictgradschool.web.passwordfun;

import java.nio.charset.Charset;
import java.util.Arrays;

public class ex03 {

        public  static final String passwordHack = "fDHMQyrtWetcUqbOcycWvVPJ1lhsNNGeZdpVkO5FPyFPpBP4x6f8+6inFvfuUYY8ipwgJMG3v7zJlZKQFZ6Q6g==";
        public static  final int MAX_PASS_LENGTH = 3;
        public static final int iterations = 500;

        public static final String[] CHARACTER_SET = "abcdefghijklmnopqrstuvwzyz0123456789".split("");

        public static byte[] myByte = passwordHack.getBytes();



        public static void main(String[] args) {

            int[] charIndex = new int[MAX_PASS_LENGTH];
            Arrays.fill(charIndex, -1);
            Thread t = null;
            String password = null;
            long startTime = System.currentTimeMillis();
            while ((password = getNextPassword(CHARACTER_SET, charIndex)) != null) {
                String finalPassword = password;
                t = new Thread(new Runnable(){
                    @Override
                    public void run() {
                        System.out.println("Password undergoing test= " + finalPassword);
                        for (byte i = Byte.MIN_VALUE; i < Byte.MAX_VALUE; i++) {
                            char[] c = finalPassword.toCharArray();

                            byte[] b = new byte[]{i};

                            byte[] p = Passwords.base64Decode(passwordHack);

                            if (Passwords.isExpectedPassword(c, b, iterations, p)) {
                                System.out.println("The password is " + finalPassword);
                                break;
                            }
                        }
                    }
                });
                t.start();


            }
            long endTime = System.currentTimeMillis();
            System.out.println(endTime-startTime);




        }

        private static String getNextPassword(String[] characterSet, int[] charIndex) {
            for (int i = 0; i < charIndex.length; i++) {
                charIndex[i] = charIndex[i] + 1;

                if (charIndex[i] >= characterSet.length) {
                    if ((i + 1) == charIndex.length) {
                        return null;
                    }

                    charIndex[i] = 0;
                } else {
                    break;
                }
            }

            String passwordUndertest = "";
            for (int aCharIndex : charIndex) {
                if (aCharIndex != -1) {
                    passwordUndertest += characterSet[aCharIndex];
                }
            }
            return passwordUndertest;
        }

    }


