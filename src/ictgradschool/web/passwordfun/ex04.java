package ictgradschool.web.passwordfun;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;

public class ex04 {
    public static  final int MAX_PASS_LENGTH = 5;

    public static final String[] CHARACTER_SET = "abcdefghijklmnopqrstu".split("");

    public static byte[] myByte;



    public static void main(String[] args) {

        int[] charIndex = new int[MAX_PASS_LENGTH];
        Arrays.fill(charIndex, -1);

        String password = null;
        HashMap<String, String> myHashMap = new HashMap<>();
        long startTime = System.currentTimeMillis();
        while ((password = getNextPassword(CHARACTER_SET, charIndex)) != null) {
            myByte = Passwords.insecureHash(password);
            String myBytes = Passwords.base64Encode(myByte);
            myHashMap.put( myBytes,password);
        }
        try (BufferedReader r = new BufferedReader(new FileReader("hash_input"))) {
            String n;
            while ((n = r.readLine()) != null) {
                System.out.println(n + " --> " + myHashMap.get(n));
            }
            long endTime = System.currentTimeMillis();
            System.out.println(endTime-startTime);


        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }







    private static String getNextPassword(String[] characterSet, int[] charIndex) {
        for (int i = 0; i < charIndex.length; i++) {
            charIndex[i] = charIndex[i] + 1;

            if (charIndex[i] >= characterSet.length) {
                if ((i + 1) == charIndex.length) {
                    return null;
                }

                charIndex[i] = 0;
            } else {
                break;
            }
        }

        String passwordUndertest = "";
        for (int aCharIndex : charIndex) {
            if (aCharIndex != -1) {
                passwordUndertest += characterSet[aCharIndex];
            }
        }
        return passwordUndertest;
    }

}


