package ictgradschool.web.passwordfun;

import com.sun.xml.internal.fastinfoset.util.CharArray;

import java.nio.charset.Charset;
import java.util.Arrays;

public class ex02{
    public static final String passwordHack = "jUwKgSbr5K0A8lk9Nq7uY2CIlflZmc8h9U5cDCeEEtwKKST6QEg6uMY+Gz11ytUDqmZM5gtwWrekaaxsQRHtPw==";
    public static final int MAX_PASS_LENGTH = 4;

    public static final int iterations = 5;

    public static final String[] CHARACTER_SET = "abcdefghijklmnopqrstuvwzyz0123456789".split("");

    public static byte[] myByte = passwordHack.getBytes();

    public static long startTime = System.currentTimeMillis();

    public static void main(String[] args) {

        int[] charIndex = new int[MAX_PASS_LENGTH];
        Arrays.fill(charIndex, -1);

        String password = null;
        outerloop:
        while ((password = getNextPassword(CHARACTER_SET, charIndex)) != null) {
            for (byte i = Byte.MIN_VALUE; i < Byte.MAX_VALUE; i++){
               char[] c = password.toCharArray();

               byte[] b = new byte[] {i} ;

               byte[] p = Passwords.base64Decode(passwordHack);

               if(Passwords.isExpectedPassword(c,b,iterations,p)){
                   System.out.println("The password is " + password);
                   break outerloop;
               }

            }

        }
        long endTime = System.currentTimeMillis();
        System.out.println(endTime-startTime);


    }





    private static String getNextPassword(String[] characterSet, int[] charIndex) {
        for (int i = 0; i < charIndex.length; i++) {
            charIndex[i] = charIndex[i] + 1;

            if (charIndex[i] >= characterSet.length) {
                if ((i + 1) == charIndex.length) {
                    return null;
                }

                charIndex[i] = 0;
            } else {
                break;
            }
        }

        String passwordUndertest = "";
        for (int aCharIndex : charIndex) {
            if (aCharIndex != -1) {
                passwordUndertest += characterSet[aCharIndex];
            }
        }
        return passwordUndertest;
    }

}
