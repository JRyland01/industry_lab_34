package ictgradschool.web.passwordfun;

import java.nio.charset.Charset;
import java.util.Arrays;

public class PasswordGeneratorProgram {
    public  static final String passwordHack = "6XZfhbfU8Il1CSHnO9NimnYMH+emHWLz1p08T+2T/8ZlXmhN6uCATkckXvdR8eBE9MlEQ4TfCDJ/ydjODM6c2g==";
    public static  final int MAX_PASS_LENGTH = 5;

    public static final String[] CHARACTER_SET = "abcdefghijklmnopqrstuvwzyz0123456789".split("");

    public static byte[] myByte = passwordHack.getBytes();



    public static void main(String[] args) {

        int[] charIndex = new int[MAX_PASS_LENGTH];
        Arrays.fill(charIndex, -1);

        String password = null;
        long startTime = System.currentTimeMillis();
        while ((password = getNextPassword(CHARACTER_SET, charIndex)) != null) {
            if(Passwords.isInsecureHashMatch(password,Passwords.base64Decode(passwordHack))){

                System.out.println("Your password is "+password);
                long endTime = System.currentTimeMillis();
                System.out.println("It took " + (endTime - startTime)/1000.00 + " second");
                break;
            }


        }

    }

    private static String getNextPassword(String[] characterSet, int[] charIndex) {
        for (int i = 0; i < charIndex.length; i++) {
            charIndex[i] = charIndex[i] + 1;

            if (charIndex[i] >= characterSet.length) {
                if ((i + 1) == charIndex.length) {
                    return null;
                }

                charIndex[i] = 0;
            } else {
                break;
            }
        }

        String passwordUndertest = "";
        for (int aCharIndex : charIndex) {
            if (aCharIndex != -1) {
                passwordUndertest += characterSet[aCharIndex];
            }
        }
        return passwordUndertest;
    }

}
